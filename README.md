# Arbroretum

Arboretum est une interface de consultation des données ouvertes sur les populations d'arbres de la ville de Saint Quentin.

### Init
Le site de l'agglomération de Saint Quentin propose un accès ouvert à certaines données.  
On peut notamment y retrouver la population d'arbres, avec les coordonnées GPS de chaque individu, ainsi que l'espèce à laquelle il appartient, le tout sous format JSON.


### Fonctionalités
Le programme génère une page HTML permettant une recherche fluide sur les données contenues dans le JSON d'entrée, une fois triées et ordonnées, avec un lien GoogleMaps et QwantMaps pour chaque individu listé.


### Utilisation
Pour exemple, on souhaite obtenir un Prunier 'Monsieur hâtif', dont les fruits recouverts d'un velours pourpres renferment une chair juteuse et commestible. En somme, un oeuf de Fabergé de la nature !

On va donc rechercher parmi les différents pruniers à l'entour, et découvrir qu'il en existe un au verger d'Isle.

![Exemple de recherche](https://raw.githubusercontent.com/cdivry/arboretum/master/img/illustration.png)

*Pif paf pouf, problème résolu !*  
*la localisation étant connue,*  
*allons ramasser quelques fruits chus !*  


### Annexes
* [Saint-Quentin.fr](https://www.saint-quentin.fr/1245-open-data.htm) L'agglomération et sa politique de libération des données publiques.
* [Agglo StQ](http://opendata.agglo-saintquentinois.fr/) Le portail Open Data de la ville de Saint Quentin.
* [Agglo StQ](http://opendata.agglo-saintquentinois.fr/datasets/arbres/data) Les données de population d'arbres.
